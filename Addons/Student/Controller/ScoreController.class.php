<?php
/**
 * Created by PhpStorm.
 * User: 雷霆UPC
 * Date: 2016/7/31
 * Time: 21:17
 */
namespace Addons\Student\Controller;

use Home\Controller\AddonsController;

class ScoreController extends AddonsController
{
    public function lists()
    {
        //$map['']
        $list = M ('student_score')->where()->select();
        $this->assign ('list',$list);
    }

    public function add()
    {
        $column =array('A'=>'code','B'=>'name','C'=>'2','D'=>'3','E'=>'4');
        vendor ( 'PHPExcel' );
        vendor ( 'PHPExcel.PHPExcel_IOFactory' );
        vendor ( 'PHPExcel.Reader.Excel5' );
        $filename = 'C:\wamp\www\weiphp3/Uploads/Download/1.xlsx';
        switch (strtolower ( 'xlsx' )) {
            case 'csv' :
                $format = 'CSV';
                $objReader = \PHPExcel_IOFactory::createReader ( $format )->setDelimiter ( ',' )->setInputEncoding ( 'GBK' )->setEnclosure ( '"' )->setLineEnding ( "\r\n" )->setSheetIndex ( 0 );
                break;
            case 'xls' :
                $format = 'Excel5';
                $objReader = \PHPExcel_IOFactory::createReader ( $format );
                break;
            default :
                $format = 'excel2007';
                $objReader = \PHPExcel_IOFactory::createReader ( $format );
        }

        $objPHPExcel = $objReader->load ( $filename );
        $objPHPExcel->setActiveSheetIndex ( 0 );
        $sheet = $objPHPExcel->getSheet ( 0 );
        $highestRow = $sheet->getHighestRow (); // 取得总行数
        for($j = 1; $j <= $highestRow; $j ++) {
            $addData = array ();
            foreach ( $column as $k => $v ) {
                    $addData [$v] = trim ( ( string ) $objPHPExcel->getActiveSheet ()->getCell ( $k . $j )->getValue () );
            }
            $isempty = true;
            foreach ( $column as $v ) {
                $isempty && $isempty = empty ( $addData [$v] );
            }

            if (! $isempty)
                $result [$j] = $addData;
        }
        $header = array_shift($result);
        $num = count($header);
        for ($i=2;$i <= $num; $i++){
            $map['name']=$header[$i];
            $map['status']='1';
            $info =M('student_subject')->where($map)->find();
            if($info){
                $subject[$i]['id'] = $info['id'];
                $subject[$i]['kname'] = 'sub'.$info['id'];
            }
        }

        foreach ($result as $key => $value){
            foreach ($value as $k => $v) {
                if($k=='code'){
                    $map['student_id'] = $v;
                }
                if ($subject[$k]) {
                    $map[$subject[$k]['kname']] = $v;
                }
            }
            $map['status']='1';
            M('student_score')->add($map);
        }
    }

    public function lis(){

    }
}