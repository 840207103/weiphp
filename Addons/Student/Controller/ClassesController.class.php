<?php
/**
 * Created by PhpStorm.
 * User: 雷霆UPC
 * Date: 2016/7/30
 * Time: 23:59
 */

namespace Addons\Student\Controller;

use Home\Controller\AddonsController;

class ClassesController extends AddonsController
{
    public function lists() {
        $page = I ( 'p', 1, 'intval' ); // 默认显示第一页数据
        $row = empty ( $model ['list_row'] ) ? 20 : $model ['list_row'];
        $map['status']='1';
        $count = M ( 'student_classes' )->where($map)->count();
        $user = M ( 'student_classes' )->where($map)->page ( $page, $row )->select ();
        if ($count > $row) {
            $page = new \Think\Page ( $count, $row );
            $page->setConfig ( 'theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%' );
            $list_data ['_page'] = $page->show ();
        }
        $this->assign ($list_data);
        $this->assign ('user',$user);
        $this->display ();
    }

    public function add()
    {
        if (IS_POST) {
            $map['name'] = I('post.name',0);
            $map['create_time'] = time();
            $map['up_time'] = time();
            $map['status'] = 1;
            $user = M('student_classes')->add($map);
            $this->assign($user);
            $this->success('修改成功', 'index.php?s=addon/Student/classes/lists');
            #$this->display();
        }
        else{
            $classes = M('student_classes')->select();
            $this->display();
        }
    }

    /**
     * 添加数据
     */
    public function update()
    {
        if (IS_POST) {
            $id = I('post.id',0);
            $map['name'] = I('post.name','');
            $map['sort'] = I('post.sort',0);
            $map['status'] = 1;
            $user = M('student_classes')->where("id=$id")->save($map);
            $this->assign('user',$user);
            $this->success('修改成功', 'index.php?s=addon/Student/classes/lists');
        }
        else{
            $id = I('get.id',0);
            if($id){
                $info = $this->getClasses($id);
                $this->assign('info',$info);
                $this->display();
            }
            else{
                //id不存在是显示错误提示返回上一页
            }
        }
    }

    /**
     * @return int
     */
    public function delete()
    {
        $id = I('get.id',0);
        //  die;
        if(M('student_subject')->where("id=$id")->delete()){
            $this->success('删除成功', 'index.php?s=/addon/Student/classes/lists');
        }
        else{
            $this->success('删除失败', 'index.php?s=/addon/Student/classes/lists');
        }
    }

    public function getClasses($id=0){
        $info = M('student_classes')->where("id=$id")->find();
        return $info;
    }

}