<?php

namespace Addons\Student\Controller;

use Home\Controller\AddonsController;

class StudentController extends AddonsController{
    /**
     * 显示学生列表数据
     */
    public function lists() {
        $page = I ( 'p', 1, 'intval' ); // 默认显示第一页数据
        $row = empty ( $model ['list_row'] ) ? 20 : $model ['list_row'];
        $row  = 1;
        $count = M ( 'student' )->count();
        $px = C ( 'DB_PREFIX' );
        $map='';
        $user = M ()->table ( $px . 'student as f' )->join ( $px . 'student_classes as u ON f.class_id=u.id' )->where ( $map )->page ( $page, $row )->select ();
        if ($count > $row) {
            $page = new \Think\Page ( $count, $row );
            $page->setConfig ( 'theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%' );
            $list_data ['_page'] = $page->show ();
        }
        $this->assign ($list_data);
        $this->assign ('user',$user);
        $this->display ();
    }

    /**
     * 添加学生数据
     */
    public function addstudent()
    {
        if (IS_POST) {
            $date = new \DateTime('NOW');
            $map['name'] = I('get.name',0);
            $map['sex'] = I('get.sex',0);
            $map['birthday'] = I('get.birthday','');
            $map['grade'] = I('get.grade',0);
            $map['class_id'] = I('get.class_id',0);
            $map['f_name'] = I('get.f_name','');
            $map['f_tel'] = I('get.f_tel','');
            $map['create_time'] = date_format($date,'Y-m-d H:i:s');
            $map['status'] = I('get.name',0);
            $user = M('student')->add($map);
            $this->assign($user);
            $this->display();
        }
        else{
            $classes = M('student_class')->select();
            $this->display();
        }
    }

    /**
     * 添加学生数据
     */
    public function updateStudent()
    {
        if (IS_POST) {
            $date = new \DateTime('NOW');
            $id = I('post.id',0);
            $map['name'] = I('post.name','');
            $map['sex'] = I('post.sex',0);
            $map['birthday'] = I('post.birthday','');
            $map['class_id'] = I('post.class_id',0);
            $map['f_name'] = I('post.f_name','');
            $map['f_tel'] = I('post.f_tel','');
            $map['create_time'] = date_format($date,'Y-m-d H:i:s');
            $map['status'] = 1;
          //  die;
            $user = M('student')->where("id=$id")->save($map);
            $this->assign('user',$user);
            $this->success('修改成功', 'index.php?s=addon/Student/Student/lists');
        }
        else{
            $id = I('get.id',0);
            if($id){
                $info = $this->getStudent($id);
                $this->assign('info',$info);
                $classes = M('student_classes')->select();
                $this->assign('classes',$classes);
                $this->display();
            }
            else{
                //id不存在是显示错误提示返回上一页
            }
        }
    }

    /**
     * @return int
     */
    public function deleteStudent()
    {
        $date = new \DateTime('NOW');
        $id = I('get.id',0);
        //  die;
        if(M('student')->where("id=$id")->delete()){
            $this->success('删除成功', 'addon/Student/Student/list');
        }
        else{
            $this->success('删除失败', 'addon/Student/Student/list');
        }
    }

    public function getStudent($id=0){
        $info = M('student')->where("id=$id")->find();
        return $info;
    }

    public function changeGroup() {
        $uids = array_unique ( ( array ) I ( 'id', 0 ) );

        if (empty ( $uids )) {
            $this->error ( '请选择用户!' );
        }
        $group_id = I ( 'group_id', 0 );
        if (empty ( $group_id )) {
            $this->error ( '请选择用户组!' );
        }
        D ( 'Home/AuthGroup' )->move_group ( $uids, $group_id );
        foreach ( $uids as $uid ) {
            D ( 'Common/User' )->getUserInfo ( $uid, true );
        }
        echo 1;
    }

    function jump($url, $msg) {
        $this->assign ( 'url', $url );
        $this->assign ( 'msg', $msg );
        $this->display ( 'jump' );
        exit ();
    }
}
