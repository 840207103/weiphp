<?php

namespace Addons\Student\Model;
use Think\Model;

/**
 * student模型
 */
class ScoreModel extends Model
{
    protected $tableName = 'student_score';

    public function info($uid = 0, $testid = 0)
    {
        $testMap['id']=$testid;
        M('student_subject')->select();
        M('student_test')->where($testMap)->select();
        $scoreMap['score']=array('>=',0);
        $scoreMap['testid']=$testid;
        M('student_score')->where($scoreMap)->select();
    }

    /**
     * @param array $scope
     */
    public function setScope($scope)
    {
        $this->_scope = $scope;
    }
}
